# frozen_string_literal: true

class StringCalculator
  def self.add(numbers)
    new(numbers).add
  end

  def initialize(numbers)
    @numbers = numbers
  end

  def add
    return 0 if @numbers.empty?

    delimiter = ','
    if @numbers.start_with?('//')
      delimiter = @numbers[2]
      @numbers = @numbers.split("\n", 2)[1]
    end

    @numbers = @numbers.gsub(/\n/, delimiter)

    nums = @numbers.split(delimiter).map(&:to_i)

    negatives = nums.select { |num| num < 0 }
    if negatives.any?
      raise "negative numbers not allowed #{negatives.join(',')}"
    end

    nums.sum
  end
end
