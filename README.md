# String Calculator

This project implements a simple string calculator in Ruby that follows specific requirements, including handling different delimiters, supporting new lines between numbers, and throwing exceptions for negative numbers.

## Execution Steps

1. **Clone the Repository**: Clone this repository using command `git clone git@gitlab.com:p7388/add-numbers.git` to your local machine.

2. **Navigate to the Project Directory**: Open a terminal and change directory to the root of the cloned repository.

3. **Run the Tests**: Execute the RSpec tests to ensure that the code functions correctly.
   ```bash   
   rspec

4. Run RuboCop (Optional): Optionally, you can run RuboCop to ensure code style compliance.
   ```bash
   rubocop

